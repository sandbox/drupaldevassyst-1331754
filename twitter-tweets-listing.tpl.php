<?php
// $Id: twitter-tweets-listing.tpl.php,v 1.1.2.2 2011/11/02 17:01:56 ronyg $

/**
 * @file
 * Theme template for a list of tweets.
 *
 * Available variables in the theme include:
 *
 * 1) An array of $tweets, where each tweet object has:
 *   $tweet->id
 *   $tweet->username
 *   $tweet->userphoto
 *   $tweet->text
 *   $tweet->timestamp
 *
 * 2) $twitkey string containing initial keyword.
 *
 * 3) $title
 *
 * 4) An array of general parameters, it has:
 *	  $twitparam->nextpage
 *	  $twitparam->previouspage
 *	  $twitparam->tweetpage
 */ 
?>
<div class="tweets-tweet-listing">
  <?php if (!empty($title)): ?>
    <h2><?php print $title; ?></h2>
  <?php endif; ?>
  <?php if (is_array($tweets)): ?>
    <?php $tweet_count = count($tweets); ?>
    <ul class="tweets-tweet-listing">
    <?php foreach ($tweets as $tweet_key => $tweet): ?>
      <li>
        <div class="tweet-authorphoto"><img src="<?php print $tweet->userphoto; ?>" alt="<?php print $tweet->username; ?>" /></div>
        <span class="tweet-author"><?php print l($tweet->username, 'http://twitter.com/' . $tweet->username, array('attributes' => array('title' => $tweet->username))); ?></span>
		<span class="tweet-text"><?php print twitter_tweets_add_links($tweet->text); ?></span>
		<div class="tweet-time"><?php print l($tweet->time_ago, 'http://twitter.com/' . $tweet->username . '/status/' . $tweet->id, array('attributes' => array('title' => $tweetTime))); ?></div>        
        <?php if ($tweet_key < $tweet_count - 1): ?>
          <div class="tweet-divider"></div>
        <?php endif; ?>
      </li>
    <?php endforeach; ?>
    </ul>	
  <?php else: ?>
  	<div class="tweet-empty-msg">
  	  <?php print twitter_tweets_empty_message(); ?>
	</div>
  <?php endif; ?>
</div>