<?php
// $Id: twitter-tweets-listing.tpl.php,v 1.1.2.2 2010/07/06 17:01:56 ronyg $

/**
 * @file
 * Theme template for a list of tweets.
 *
 * Available variables in the theme include:
 *
 * 1) An array of $tweets, where each tweet object has:
 *   $tweet->id
 *   $tweet->username
 *   $tweet->userphoto
 *   $tweet->text
 *   $tweet->timestamp
 *
 * 2) $twitkey string containing initial keyword.
 *
 * 3) $title
 *
 * 4) An array of general parameters, it has:
 *	  $twitparam->nextpage
 *	  $twitparam->previouspage
 *	  $twitparam->tweetpage
 */ 
?>
<div class="tweets-tweet-listing">
  <?php if (!empty($title)): ?>
    <h2><?php print $title; ?></h2>
  <?php endif; ?>
  <?php if (is_array($tweets)): ?>
    <?php 
	$tweet_count = count($tweets);
    $prevpage = $twitparam->tweetpage-1;
	$prevpage = ($prevpage > 0) ? $prevpage : '';	
	$nextpage = $twitparam->tweetpage+1;
	$nextpage = (!empty($twitparam->previouspage) && empty($twitparam->nextpage)) ? '' : $nextpage;	
	?>
    <ul class="tweets-tweet-listing">
    <?php foreach ($tweets as $tweet_key => $tweet): ?>
      <li>
        <div class="tweet-authorphoto"><img src="<?php print $tweet->userphoto; ?>" alt="<?php print t($tweet->username); ?>" /></div>
        <span class="tweet-author"><?php print l(t($tweet->username), 'http://twitter.com/' . $tweet->username, array('attributes' => array('title' => t($tweet->username)))); ?></span>
		<span class="tweet-text"><?php print twitter_tweets_add_links(t($tweet->text)); ?></span>
		<div class="tweet-time"><?php print l(t($tweet->time_ago), 'http://twitter.com/' . $tweet->username . '/status/' . $tweet->id, array('attributes' => array('title' => t($tweetTime)))); ?></div>        
        <?php if ($tweet_key < $tweet_count - 1): ?>
          <div class="tweet-divider"></div>
        <?php endif; ?>
      </li>
    <?php endforeach; ?>
    </ul>
    <?php if(!empty($prevpage) || !empty($nextpage)): ?>
      <div class="tweet-morelink">	
	  <?php if ($prevpage): ?>
	    <?php print l(t('Previous'), 'twitter_tweets_page/'.$twitkey.'/'.$prevpage, array('attributes' => array('title' => t('Previous')))); ?>
	  <?php endif; ?>
	  <span class="tweet-page"><?php print t('Page ').$twitparam->tweetpage; ?></span>
	  <?php if ($nextpage): ?>
	    <?php print l(t('Next'), 'twitter_tweets_page/'.$twitkey.'/'.$nextpage, array('attributes' => array('title' => t('Next')))); ?>
	  <?php endif; ?>
      </div>
    <?php endif; ?>
  <?php else: ?>
  	<div class="tweet-empty-msg">
  	  <?php print twitter_tweets_empty_message(); ?>
	</div>
  <?php endif; ?>
</div>