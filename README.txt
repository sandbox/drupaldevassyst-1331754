Twitter Tweets Module
--------------------------------
by Ronyg

Description
----------------
This module extends the features of Twitter Pull module by using its core script.

Twitter Tweets is a small module, the only purpose of which is to allow super-easy embedding of public twitter data like: a user timeline or twitter search results by hashtag or a search term. The emphasis is on making these very specific tasks as easy and straightforward as possible.  The purpose of the module is not to be an end-all solution for Twitter. 

If you need more flexibility than just printing-out tweet list in a template file, you should write a quick glue module implementing hook_block() according to your specific application logic and calling the twitter_tweets_render() function from the hook.

Installation 
---------------
 * Copy the module's directory to your modules directory and activate the module.
 * Go to admin/settings/twitter_tweets_settings and set configuration variables.
 * Clear your Drupal cache at admin/settings/performance by clicking 'Clear cached data'.
 * Go to admin/build/block and assign "Twitter Tweets" to any region or you can create your own block by using twitter_tweets_render() function and assign it to any region.
 * Configure the assigned block(s) and save it with proper values.
 
 Advanced Use:
 ---------------------
 There're no authentication information or API Keys required. Module only interacts with non-auth APIs. In the end, it all boils down to a single function:

twitter_tweets_render ($twitkey, $title = NULL, $num_items = NULL, $themekey = NULL, $page = 1, $page_call = FALSE)

ARGUMENTS: 

$twitkey
	Twitter key, which can be a username (prepended with @) a hashtag (prepended with #) or a search keyword (no prefix).
	
$title
	Title passed to tpl.php. If you want title to be disabled, pass boolean False.
	
$num_items
	Maximum number of tweets to pull from the Twitter result-set. It is better to use 20 as its max value. 
	
$themekey
	Drupal theme key-name to use for theming the output of the Twitter response. Allows providing different tpl.php's for different use-cases. The default is: twitter-tweets-listing, which corresponds to a fully-themed twitter-tweets-listing.tpl.php in module folder. Please note that underscores become dashes in the TPL name and you may not use dashes in the key name. The tpl.php can be copied to a theme folder for further customization or modified via CSS. 

$page
	It is the current page number of twitter listing page.
	
$page_call
	It specifies whether the function call is from a page or from a block. Caching is only applicable in case of blocks. Its default value is FALSE and for the case of pages, set it to TRUE.
	
SPECIAL NOTE:

If you decide to use a custom theme_key, you will have to register it with a hook_theme in a module or theme . Unfortunately, there's not a good way for this module to do it. You should use code like following:

function yourmodule_theme() {
  return array(
    'your_custom_theme_key' => array(
      'arguments' => array('tweets' => NULL, 'twitkey' => NULL, 'title' => NULL, 'twitparam' => NULL),
      'template' => 'your-custom-theme-key'
    ),
  );
}

EXAMPLES:

<?php print twitter_tweets_render ('drupal'); ?>

will print-out nicely themed list of latest tweets from Drupal.

OR you can use with all the parameters like

<?php print twitter_tweets_render ($twitkey = 'drupal', $title = 'Twitter', $num_items = 20, $themekey = 'twitter_tweets_page_listing', $page = 1, $page_call = TRUE); ?>
	
will print-out nicely themed list of latest tweets from Drupal with 20 items per page. It will use 'twitter-tweets-page-listing.tpl.php' as template and 'Twitter' as title.